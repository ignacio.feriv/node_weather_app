const axios = require('axios');
const { response } = require('express');
const config = require('../config');
const logger = require('../loaders/logger');

class WeatherRepository {
  constructor() {
    this.units = 'metric';
    this.pathBase = config.openweathermap.base_path;
    this.appid = config.openweathermap.api_key; 
  }

  async weatherByCoords(lon, lat) {
     try {
       const instance = axios.create({
         baseURL: `${this.pathBase}`,
         params:{
            'appid': this.appid,
            'units': this.units,
            lon,
            lat 
         }
       });

       const response = await instance.get();

       return response.data
     } catch (err) {
       throw err;
     }
  }
}

module.exports = WeatherRepository;