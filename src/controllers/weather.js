const express = require('express');
const Success = require('../handlers/successHandler');
const { weatherByCoords, weatherByCityId } = require('../services/weatherService'); 

/**
 * 
 * @param {express.Request} req 
 * @param {express.Response} res 
 */
const weather = async (req, res, next) => {
  try {
    const { lon, lat } = req.query; 
    const weather = await weatherByCoords(lon, lat);
    res.json(new Success(weather));
  } catch (err) {
    next(err);
  }
}

const weatherByCity = async (req, res, next) => {
  try {
    const { city, id } = req.params; 
    const weather = await weatherByCityId(city, id);
    res.json(new Success(weather));
  } catch (err) {
    next(err)
  }
}

module.exports = { 
  weather,
  weatherByCity
}