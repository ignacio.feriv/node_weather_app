const express = require('express');
const Success = require('../handlers/successHandler');
const { findCities } = require('../services/cityService');
/**
 * 
 * @param {express.Request} req 
 * @param {express.Response} res 
 */
const cities = async (req, res, next) => {
  try {
    const city = req.params.city;
    const cities = await findCities(city);
    res.json(new Success(cities));
  } catch (err) {
    next(err);
  }
}

module.exports = { 
  cities
}