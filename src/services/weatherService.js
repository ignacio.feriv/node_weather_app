const WeatherRepository = require('../repositories/weatherRepository');
const weatherRepository = new WeatherRepository();
const CityRepository = require('../repositories/cityRepository');
const cityRepository = new CityRepository();
const logger = require('../loaders/logger');
const e = require('express');

const weatherByCoords = async(lon, lat) => {
  
  const weather = await weatherRepository.weatherByCoords(lon, lat);
  
  return {
    description: weather.weather[0].description,
    temperature: weather.main.temp,
    temperatureMin: weather.main.temp_min,
    temperatureMax: weather.main.temp_max
  }
}

const weatherByCityId = async(city, id) => {
  const cities = await cityRepository.findCities(city);

  const cityData = cities.features.filter(c => c.id === id);
  console.log(cityData[0]);
  const lon = cityData[0].geometry.coordinates[0];
  const lat = cityData[0].geometry.coordinates[1];

  return weatherByCoords(lat, lon);
}

module.exports = {
  weatherByCoords,
  weatherByCityId
};