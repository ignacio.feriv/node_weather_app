const express = require('express');
const { loggers } = require('winston');
const CityRepository = require('../repositories/cityRepository');
const repository = new CityRepository();

const findCities = async(city) => {
  
  const cities = await repository.findCities(city);
  
  return cities.features.map( c => {
    return {
      id: c.id,
      name: c.place_name,
      log: c.geometry.coordinates[0],
      lat: c.geometry.coordinates[1]
    }
  });
}

module.exports = {
  findCities
};