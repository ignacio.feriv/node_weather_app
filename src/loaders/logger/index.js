const winston = require('winston');
const config = require('../../config');
const transports = [];
const method = process.env.NODE_ENV === 'production' ? new winston.transports.File({ filename: 'error.log', level: 'error' }) : new winston.transports.Console();
transports.push(method);

const LoggerIntance = winston.createLogger({
  level: config.log.level,
  format: winston.format.simple(),
  transports
});

module.exports = LoggerIntance;