const { Router } = require('express');
const { 
  weather,
  weatherByCity
} = require('../controllers/weather');
const router = Router();

router.get('/', weather);
router.get('/:city/:id', weatherByCity);

module.exports = router;